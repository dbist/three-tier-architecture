**How to Create three-tier Application (my weekend project)**

![](./3-Tier-Server-Architecture.png)


	1- Login to aws console and launch an ec2 instance.
	2- Make sure port 80 for apache, 8080 for tomcat and DB port open in security group.
	3- Install httpd (apache) web server.
	4- Copy html sample app to /var/www/html
	5- Create student.conf Proxy file under conf.d to connect with backend tomcat server.
	6- Make sure this conf file is included in httpd.conf.
	7- Restart httpd apache server.  >>>1st Tier completed.
	8- Browse the application and test. http://ec2-3-92-133-183.compute-1.amazonaws.com/
	9- Successfully able to get the sample page is Tier1. once you click on "Enter to student Application" it will route you to Tier 2 (backend application server).
	10- Install tomcat application server for 2nd tier
	11- Copy the sample application to /etc/lib/tomcat/webapps/
	12- Start tomcat application server.
	13- Browse the application and test. http://ec2-3-92-133-183.compute-1.amazonaws.com/student
	14- once you click on "Enter to student Application" and able to get student form that mean tier 2 application server is good and responding.
	15- 2nd tire completed.
	16- Installing database server for 3rd tire….. :) still in progress…
	17- Once Tier 3 is completed, I will be able to save the data to database server.
	
	Hope this will help to you all.
	Happy Learning :)   
